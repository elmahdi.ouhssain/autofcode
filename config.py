# config.py

import os

class Config(object):
    """
    Common configurations
    """

    # Put any configurations here that are common across all environments


class DevelopmentConfig(Config):
    """
    Development configurations
    """

    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # email server
    #DEBUG = True
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

        ####LOCALHOST######################
    RECAPTCHA_PUBLIC_KEY ='6Lea-98UAAAAAKnbaxPVr6dJ3natl23pR0Q2KZYi'
    RECAPTCHA_PRIVATE_KEY ='6Lea-98UAAAAAG3PfuGzaxCHSvxue-0aQLCCuS8C'
    RECAPTCHA_OPTIONS = {'theme':'black'}
    RECAPTCHA_USE_SSL = False


class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # email server
    #DEBUG = True
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    RECAPTCHA_PUBLIC_KEY ='6Le1cOEUAAAAANu7L_xSM86lMfn4_-b0Uu2qJTBe'
    RECAPTCHA_PRIVATE_KEY ='6Le1cOEUAAAAAJ4wZb7L-AnlS_9J_W_ros0vbsAS'

    RECAPTCHA_OPTIONS = {'theme':'black'}
    RECAPTCHA_USE_SSL = True

app_config = {

    'development': DevelopmentConfig,
    'production': ProductionConfig
}