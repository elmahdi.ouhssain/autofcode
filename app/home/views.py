# -*- coding: utf-8 -*-
from flask import Blueprint, abort, flash, g, jsonify, redirect, \
	render_template, request, url_for, jsonify, json, Markup, make_response
from flask_login import login_required, login_user, current_user, logout_user
from sqlalchemy.orm import subqueryload
from sqlalchemy import desc, update, insert
from . import home
from .forms import BuyNow, EnigmaForm, TrialsForm, MagDevice, ContactForm, Getoffer, URRegistrationForm
from .. import db
from ..models import User, Setting, Package, Contact, Trial, Tuto, Subscription, Offer, Reseller, init_db, Base, engine
import requests
import json
#from ..sender import *
from threading import Thread, Lock
from flask import current_app
from app import mail, Message
from requests.exceptions import HTTPError

@home.route('/', methods=['GET', 'POST'])
def homepage():
	"""
	Render the homepage template on the / route

	"""
	packages = Package.query.order_by(desc(Package.id)).limit(3)

	return render_template('/home/index.html', packages=packages, title='Best IPTV Solution', description='URHDIPTV is One of the biggest IPTV solutions and IPTV provider in the world, aims to help you run your own IPTV service also buy most popular packages, with us you can watch worldwide channels,for free before pay')

@home.route('/subscribe/<slug>/', methods=['GET', 'POST'])
#@login_required
def subscribe(slug):
	setting = Setting.query.filter_by(id=2).first()
	x = setting.server
	y = setting.api_create
	z = setting.m3u
	pp = setting.paypal_id
	packages = Package.query.filter_by(slug=slug).one()
	form = BuyNow()
	r = packages.name
	x_id = packages.xtream_id
	packname = packages.name
	if form.validate_on_submit():
		
		url = setting.server + setting.api_create
		payload = {}
		payload['username'] = 'default'
		payload['password'] = 'defaulttest'
		payload['trial'] = '0'
		payload['reseller_notes'] = 'testanyone'
		payload['package'] = x_id
		files = []
		headers= {}
		response = requests.request("POST", url, headers=headers, data = payload, files = files)
		people_json  = response.json()
		usr = people_json['username'].format()
		pss = people_json['password'].format()
		server = setting.server
		m3u = '{}/get.php?username={}&password={}&type=m3u_plus&output=mpegts'.format(z, usr, pss)
		pack = Subscription(f_name=form.f_name.data,email=form.email.data,country_selector=form.country_selector.data,device_type=form.device_type.data,pack=r,phone_num=form.phone_num.data,username=usr,password=pss)
		db.session.add(pack)
		db.session.commit()
		msg = Message('IPTV subscription', sender="URHDIPTV | TEAM", recipients=[form.email.data.strip()])
		msg.html = render_template('/home/subs/sub.html',m3u=m3u)
		mail.send(msg)
		flash('Congratulation, you order now placed ,your subscription valid for 12MONTHS,and sent to your email', 'success')
		return redirect(url_for('home.homepage'))
	return render_template('/home/buytest.html', packages=packages, pp=pp, form=form, title=packages.name, description='With URHDIPTV Buy best IPTV services, watch worldwide channels for free')


@home.route('/subscribe/details/', methods=['GET', 'POST'])
def details():

	setting = Setting.query.filter_by(id=2).first()
	x = setting.server
	y = setting.api_create
	z = setting.m3u

	username = request.args.get('usr')
	password = request.args.get('pss')
	m3u = '{}/get.php?username={}&password={}&type=m3u_plus&output=mpegts'.format(z, username, password)
	enigma = 'wget -O /etc/enigma2/iptv.sh "http://s1.urhdiptv.info:8080/get.php?username={}&password={}&type=enigma22_script&output=mpegts" && chmod 777 /etc/enigma2/iptv.sh && /etc/enigma2/iptv.sh'.format(username, password)

	return render_template('/home/details.html', username=username, password=password, m3u=m3u, enigma=enigma, title='IPTV subsription details', description='Informations of IPTV subscription ')


@home.route('/failed', methods=['GET', 'POST'])
def failed():
	return render_template('/home/failed.html')

@home.route('/pricing/gettrial', methods=['GET', 'POST'])
@home.route('/trial', methods=['GET', 'POST'])
def trial():

	setting = Setting.query.filter_by(id=2).first()
	x = setting.server
	y = setting.api_create
	z = setting.m3u
	packages = Package.query.all()
	form = TrialsForm()
	if form.validate_on_submit():
		try:
			url = x + y 
			payload = {}
			payload['username'] = 'default'
			payload['password'] = 'defaulttest'
			payload['trial'] = '1'
			payload['reseller_notes'] = 'testanyone'
			payload['package'] = request.form.get('getidpack')
			files = []
			headers= {}
			response = requests.request("POST", url, headers=headers, data = payload, files = files)
			people_json  = response.json()
			ray = people_json
			usr = ray['username'].format()
			pss = ray['password'].format()
			m3u = '{}/get.php?username={}&password={}&type=m3u_plus&output=mpegts'.format(z, usr, pss)
			trial = Trial(country_selector=form.country_selector.data,email_rec=form.email_rec.data,device_type=form.device_type.data)
			db.session.add(trial)
			db.session.commit()
			msg = Message('Free 2H IPTV Trial', sender="URHDIPTV | TEAM", recipients=[form.email_rec.data.strip()])
			msg.html = render_template('/home/trial/sub_trial.html',m3u=m3u)
			mail.send(msg)
			flash('Congratulation, your trial is now active for 2H and sent to your email !', 'success')
			#return redirect(url_for('home.trial_details', usr=usr, pss=pss))
			return redirect(url_for('home.homepage'))
		except ValueError:
		#except KeyError:
			return redirect(url_for('home.failed'))
	return render_template('/home/trial.html', form=form, packages=packages, title='Free IPTV Trial Details', description='Request on URHDIPTV free IPTV trial for 24H up to 72H')

@home.route('/trial/details/', methods=['GET', 'POST'])
def trial_details():
	setting = Setting.query.filter_by(id=2).first()
	x = setting.server
	y = setting.api_create
	z = setting.m3u
	username = request.args.get('usr')
	password = request.args.get('pss')
	m3u = '{}/get.php?username={}&password={}&type=m3u_plus&output=mpegts'.format(z, username, password)
	enigma = 'wget -O /etc/enigma2/iptv.sh "http://s1.urhdiptv.info:8080/get.php?username={}&password={}&type=enigma22_script&output=mpegts" && chmod 777 /etc/enigma2/iptv.sh && /etc/enigma2/iptv.sh'.format(username, password)
	return render_template('/home/trial_details.html', username=username, password=password, m3u=m3u, enigma=enigma, title='IPTV Trial details', description='Informations of IPTV subscription ')

@home.route('/trial/enigma', methods=['GET', 'POST'])
def enigma():
	setting = Setting.query.filter_by(id=2).first()
	x = setting.server
	y = setting.api_create
	z = setting.m3u
	packages = Package.query.all()
	form = EnigmaForm()
	if form.validate_on_submit():
		full_name = form.full_name.data
		email_rec = form.email_rec.data
		#mac_mag = form.mac_mag.data
		url = x + y 
		payload = {}
		payload['is_e2'] = '1'
		payload['mac_address_mag'] = form.enigma_mag.data
		payload['trial'] = '1'
		payload['reseller_notes'] = 'testanyone'
		payload['package'] = request.form.get('getidpack')
		files = []
		headers= {}
		response = requests.request("POST", url, headers=headers, data = payload, files = files)
		people_json  = response.json()
		ray = people_json
		usr = ray['username'].format()
		pss = ray['password'].format()
		m3u = '{}/get.php?username={}&password={}&type=m3u_plus&output=mpegts'.format(z, usr, pss)
		msg = Message('Free 2H IPTV Trial', sender="URHDIPTV | TEAM", recipients=[form.email_rec.data.strip()])
		msg.html = render_template('/home/trial/sub_trial.html',m3u=m3u)
		mail.send(msg)
		flash(Markup('Success Mr: {}, IPTV trial sent to your Enigma device. Please Use the portal : http://s1.urhdiptv.info:8080/c . and check your email for more details').format(full_name), 'success')
		#return redirect(url_for('home.trial_details', usr=usr, pss=pss))
		return redirect(url_for('home.homepage'))
	return render_template('/home/trial/enigma.html', form=form, packages=packages, title='Free IPTV Trial Details For Enigma', description='Request on URHDIPTV free IPTV trial for 24H up to 72H')



#########################################################################################################""
@home.route('/trial/mag', methods=['GET', 'POST'])
def mag():
	setting = Setting.query.filter_by(id=2).first()
	x = setting.server
	y = setting.api_create
	z = setting.m3u
	form = MagDevice()
	if form.validate_on_submit():
		full_name = form.full_name.data
		#mac_mag = form.mac_mag.data
		url = x + y 
		payload = {}
		payload['is_mag'] = '1'
		payload['mac_address_mag'] = form.mac_mag.data
		payload['trial'] = '1'
		payload['reseller_notes'] = 'testanyone'
		payload['package'] = '3'
		files = []
		headers= {}
		response = requests.request("POST", url, headers=headers, data = payload, files = files)
		mag_json  = response.json()
		msg = Message('Free 2H IPTV Trial for MAG device', sender="URHDIPTV | TEAM", recipients=[form.email_rec.data.strip()])
		msg.body = "Use the portal for login : http://s1.urhdiptv.info:8080/c"
		mail.send(msg)
		flash(Markup('Success Mr: {}, IPTV trial sent to your Mag device. Please Use the portal : http://s1.urhdiptv.info:8080/c').format(full_name), 'success')
		return redirect(url_for('home.homepage'))
	return render_template('/home/trial/mag.html', form=form, title='Get free trial for mag device')

################################################################################################
@home.route('/contact', methods=['GET', 'POST'])
def contact():

	form = ContactForm()
	if form.validate_on_submit():
		contact = Contact(name=form.name.data,mail=form.mail.data,sub=form.sub.data,msg=form.msg.data)
		db.session.add(contact)
		db.session.commit()
		#flash('Your Message has been sent ;)', 'success')
		flash('Your Message has been sent Mr : {}'.format(contact.name), 'success')
		# redirect to the login page
		return redirect(url_for('home.homepage'))
	return render_template('/home/contact.html', form=form, title='Contact URHDIPTV team', description='If you have any enquiry or sales just contact URHDIPTV TEAM')


@home.route('/apps', methods=['GET', 'POST'])
def app():

	return render_template('/home/app.html', title='Best IPTV application', description='URHDIPTV Offer Best and free IPTV application to watch world wide live stream envents')

@home.route('/offers', methods=['GET', 'POST'])
def offer():

	offers = Offer.query.order_by(Offer.of_time.desc()).limit(5)

	return render_template('/home/offer.html', offers=offers, title='New IPTV Offers', description='Special IPTV offers for resellers, subresellers with full access controle panel to manage subscriptions')


@home.route('/offers/<slug>/', methods=['GET', 'POST'])
#@login_required
def offer_selected(slug):

	offers = Offer.query.filter_by(slug=slug).first()
	r = offers.title
	fast_form = URRegistrationForm()
	form = Getoffer()
	if request.method == 'POST':
		if form.validate_on_submit():
			taked = Reseller(name=form.name.data,whatsapp=form.whatsapp.data,country_selector=form.country_selector.data,take=r,user_id=current_user.id)
			db.session.add(taked)
			db.session.commit()
			flash('We will contact on whatsapp: {} or via email as soon possible'.format(form.whatsapp.data), 'success')
			return redirect(url_for('home.sucess'))
		elif fast_form.validate_on_submit():
			user = User(email=fast_form.email.data,username=fast_form.username.data,password=fast_form.password.data)
			db.session.add(user)
			db.session.commit()
			flash('You are now registered and login as {} . you can complete now , please fill the form bellow and place your payment !'.format(fast_form.username.data), 'success')
			login_user(user)
			
			# redirect to the login page
			return redirect(url_for('home.offer_selected', slug=slug))
	return render_template('/home/getting_offer.html', offers=offers, fast_form=fast_form, form=form, title='New IPTV Offers', description='Take this offer as fast possible before limit')


@home.route('/subscribe/success/', methods=['GET', 'POST'])
def sucess():

	return render_template('/home/thanks.html', title='Congratulation your purchase is nox complet', description='Your IPTV order Is now Complete')

#@home.route('/offers/fast_register/', methods=['GET', 'POST'])

#############################################OTHER PAGES######""""""""""""""""""""""""""""""""""""""""""""""""""
@home.route('/terms', methods=['GET', 'POST'])
def terms():


	return render_template('/home/term.html', title='Term Of service', description='Term of our services')

@home.route('/privacy', methods=['GET', 'POST'])
def privacy():


	return render_template('/home/privacy.html', title='Privacy Policy', description='Our Privacy Policy')

@home.route('/refund', methods=['GET', 'POST'])
def refund():


	return render_template('/home/refund.html', title='Refund Policy', description='Our Refund Policy')
	
@home.route('/pricing/reseller/', methods=['GET', 'POST'])
@home.route('/feautures', methods=['GET', 'POST'])
def soon():


	return render_template('/home/soon.html', title='New IPTV feautures', description='Comming Soon')
###########################################################################################################

@home.route('/blog/', methods=['GET', 'POST'])
@home.route('/tutorials/', methods=['GET', 'POST'])
def tutorial():

	#page = request.args.get('page', 1, type=int)
	#tutorials = Tuto.query.order_by(Tuto.tuto_time.desc()).paginate(page=page, per_page=5)
	#tutorials = Tuto.query.order_by(desc(Tuto.id)).limit(5)
	tutorials = Tuto.query.order_by(Tuto.tuto_time.desc()).limit(5)
 

	return render_template('/home/tuto.html', title='New IPTV solutions & tutorials', tutorials=tutorials, description='New Tutorials of IPTV installation, and IPTV problems')

@home.route('/tutorials/<slug>/', methods=['GET', 'POST'])
def tuto_post(slug):

	tutorials = Tuto.query.filter_by(slug=slug).first()

	return render_template('/home/tuto_post.html', title=tutorials.title, tags=tutorials.tags, tutorials=tutorials, description='New Tutorials of IPTV installation, and IPTV problems')
###########################################################################################################
