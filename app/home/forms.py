from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, validators, SelectField, TextAreaField, DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from flask_ckeditor import CKEditorField 
from ..models import User, User, Package, Contact, Trial, Tuto, Subscription, Offer

class URRegistrationForm(FlaskForm):
	email = StringField(validators=[DataRequired(), Email()])
	username = StringField(validators=[DataRequired()])
	password = PasswordField(validators=[
											DataRequired(),
											validators.Length(min=10)
											])

	#confirm_password = PasswordField('Confirm Password')

	recaptcha = RecaptchaField()
	submit = SubmitField('Register')

	def validate_email(self, email):
		user = User.query.filter_by(email=email.data).first()
		if user is not None:
			raise ValidationError('Please use a different email.')

	def validate_username(self, username):
		user = User.query.filter_by(username=username.data).first()
		if user is not None:
			raise ValidationError('Please use a different username.')

class TrialsForm(FlaskForm):
	country_selector = StringField('Country : ', validators=[DataRequired()])
	email_rec = StringField('Email : ', validators=[DataRequired(), Email()])
	device_type = SelectField('Device Type : ', choices=[('Android', 'Android'), ('Smart Tv', 'Smart Tv'), ('Receiver', 'Receiver'), ('Apple Tv', 'Apple Tv'), ('PC', 'PC')])
	recaptcha = RecaptchaField()
	#submit = SubmitField('Request')

	def validate_email_rec(self, email_rec):
		trial = Trial.query.filter_by(email_rec=email_rec.data).first()
		if trial is not None:
			raise ValidationError('Trial already sent to you , please check your inbox.')

class MagDevice(FlaskForm):
	full_name = StringField('Full name : ', validators=[DataRequired()])
	mac_mag = StringField('Mac Addresse : ', validators=[DataRequired()])
	email_rec = StringField('Email : ', validators=[DataRequired(), Email()])
	recaptcha = RecaptchaField()

	#submit = SubmitField('Request')

class EnigmaForm(FlaskForm):
	full_name = StringField('Full name : ', validators=[DataRequired()])
	email_rec = StringField('Email : ', validators=[DataRequired(), Email()])
	enigma_mag = StringField('Mac Addresse : ', validators=[DataRequired()])
	recaptcha = RecaptchaField()
	#submit = SubmitField('Request')

	def validate_email_rec(self, email_rec):
		trial = Trial.query.filter_by(email_rec=email_rec.data).first()
		if trial is not None:
			raise ValidationError('Trial already sent to you , please check your inbox.')

class Getoffer(FlaskForm):

	#email_rec = StringField('Email', validators=[DataRequired(), Email()])
	name = StringField('Full name', validators=[DataRequired()])
	whatsapp = StringField('Phone | Whatsapp', validators=[DataRequired()])
	country_selector = StringField('Country', validators=[DataRequired()])
	#recaptcha = RecaptchaField()
	#submit = SubmitField('GetNow')

class BuyNow(FlaskForm):

	f_name = StringField('Full Name')
	email = StringField('Email', validators=[DataRequired(), Email()])
	country_selector = StringField('Country : ')
	device_type = SelectField('Device Type', choices=[('ANDROID', 'Android'), ('SMART TV', 'Smart Tv'), ('RECEIVER', 'Receiver'), ('APPLE TV', 'Apple Tv'), ('PC', 'PC'), ('Mag', 'Mag'), ('Enigma', 'Enigma')])
	phone_num = StringField('Phone Number')
	#note = TextAreaField('Note About Order')
	#recaptcha = RecaptchaField()
	#submit = SubmitField('Checkout')



class ContactForm(FlaskForm):

	name = StringField('Full Name', validators=[DataRequired()])
	mail = StringField('Email', validators=[DataRequired(), Email()])
	sub = StringField('Subject', validators=[DataRequired()])
	msg = TextAreaField('Message', validators=[DataRequired(), Length(min=4, message=('Your message is too short.'))])
	recaptcha = RecaptchaField()

	#submit = SubmitField('Send')
