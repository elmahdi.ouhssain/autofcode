from datetime import datetime
from sqlalchemy import create_engine, Column, TEXT, Integer, String, Boolean, DateTime, \
	 ForeignKey, event, TypeDecorator, Boolean, TypeDecorator, event
from sqlalchemy.orm import scoped_session, sessionmaker, backref, relationship
from sqlalchemy.ext.declarative import declarative_base
from flask import flash, g, jsonify
from slugify import slugify
from sqlalchemy.orm import scoped_session, sessionmaker, backref, relationship
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login_manager
import pymysql
from sqlalchemy import Boolean, TypeDecorator, event
import hashlib

engine = create_engine('mysql+pymysql://dbtest_urhd22:dbtest_urhd22@localhost:3306/dbtest_urhd22?charset=utf8mb4&binary_prefix=true', pool_pre_ping=True)

#mysql+pymysql://acomplexbc:acomplexbc@localhost/acomplexbc?charset=utf8mb4&binary_prefix=true

db.session = scoped_session(sessionmaker(autocommit=False,
										 autoflush=False,
										 bind=engine))

Base = declarative_base()
Base.query = db.session.query_property()

def init_db():
	Model.metadata.create_all(bind=engine)

Model = declarative_base(name='Model')
Model.query = db.session.query_property()

class Optimization(db.Model):
	__tablename__ = 'optimizations'

	id = db.Column(db.Integer, primary_key=True)
	google_wbm = db.Column(db.TEXT, nullable=True)
	bing_wbm = db.Column(db.TEXT, nullable=True)
	logo_off = db.Column(db.String(400), nullable=True)
	sec_logo = db.Column(db.String(400), nullable=True)

	last_check = db.Column(DateTime, default=datetime.utcnow())
	def __init__(self,  google_wbm,bing_wbm,logo_off,sec_logo):
		super(Optimization, self).__init__()
		self.google_wbm = google_wbm
		self.bing_wbm = bing_wbm
		self.logo_off = logo_off
		self.sec_logo = sec_logo

class Page(db.Model):
	__tablename__ = 'pages'
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(100), nullable=True)
	content = db.Column(db.TEXT, nullable=True)
	description = db.Column(db.TEXT, nullable=True)
	keywords = db.Column(db.TEXT, nullable=True)
	is_offline = db.Column(Boolean, default=False)
	slug = db.Column(db.String(180), nullable=False)
	last_check = db.Column(DateTime, default=datetime.utcnow())
	def __init__(self, title, content,description,keywords, is_offline):
		super(Page, self).__init__()
		self.title = title
		self.content = content
		self.description = description
		self.keywords = keywords
		self.is_offline = is_offline
		self.slug = slugify(title)
	


class Setting(db.Model):
	__tablename__ = 'settings'
	id = db.Column(db.Integer, primary_key=True)
	api_infos = db.Column(db.String(400), nullable=True)
	api_create = db.Column(db.String(400), nullable=True)
	api_mag = db.Column(db.String(400), nullable=True)
	api_enigma = db.Column(db.String(400), nullable=True)
	m3u = db.Column(db.String(400), nullable=True)
	server = db.Column(db.String(400), nullable=True)

	paypal_id = db.Column(db.String(400), nullable=True)

	last_check = db.Column(DateTime, default=datetime.utcnow())
	def __init__(self, api_infos,api_create,api_mag,api_enigma,m3u,server, paypal_id):
		super(Setting, self).__init__()
		self.api_infos = api_infos
		self.api_create = api_create
		self.api_mag = api_mag
		self.api_enigma = api_enigma
		self.m3u = m3u
		self.server = server
		self.paypal_id = paypal_id


class User(UserMixin, db.Model):

	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(60), unique=True)
	username = db.Column(db.String(60), unique=True)
	password_hash = db.Column(db.String(255))
	credit = db.Column(db.Integer, default=10)
	is_admin = db.Column(Boolean, default=False)
	is_reseller = db.Column(Boolean, default=False)
	is_subscriber = db.Column(Boolean, default=False)
	affiliate_code = db.Column(db.String(90), index=False)
	###########Tracking################
	last_login_at = db.Column(DateTime())
	current_login_at = db.Column(DateTime())
	last_login_ip = db.Column(db.String(100))
	current_login_ip = db.Column(db.String(100))
	login_count = db.Column(db.Integer)
	###########Tracking################
	####################################
	channel = db.Column(db.String(90), index=False)
	mac = db.Column(db.String(90), unique=True)
	m3u = db.Column(db.TEXT)
	email_confirmation_sent_on = db.Column(DateTime, nullable=True)
	email_confirmed = db.Column(Boolean, nullable=True, default=False)
	email_confirmed_on = db.Column(DateTime, nullable=True)
  #############Relashionships##########################################
  ###############resellers############
	
	tickets = db.relationship("Support", backref='users', lazy='dynamic')
	tutos = db.relationship('Tuto', backref='users', lazy='dynamic')
	subscriptions = db.relationship("Subscription", backref='users', lazy='dynamic')
	offers = db.relationship("Offer", backref='offer', lazy='dynamic')
	created_at = db.Column(DateTime, default=datetime.utcnow())
	resellers = relationship("Reseller", uselist=False, backref="users")
	temps = db.relationship("Temps", backref='temp', lazy='dynamic')
	comments = db.relationship("Comment", backref='comments', lazy='dynamic')

	@property
	def password(self):
		"""
		Prevent pasword from being accessed
		"""
		raise AttributeError('password is not a readable attribute.')

	@password.setter
	def password(self, password):
		"""
		Set password to a hashed password
		"""
		#pepper = os.environ.get('SECRET_SALT')
		self.password_hash = generate_password_hash(password)

	def verify_password(self, password):
		
		return check_password_hash(self.password_hash, password)

	def __init__(self, email, username, password, is_admin=False, is_reseller=False, is_subscriber=False): #admin=False
		super(User, self).__init__()
		self.email = email
		self.username = username
		self.password = password
		self.email_confirmation_sent_on = None
		self.email_confirmed = False
		self.email_confirmed_on = None
		self.last_login_at = None
		self.current_login_at = datetime.now()
		self.is_admin = is_admin
		self.is_reseller = is_reseller
		self.is_subscriber = is_subscriber

	def is_authenticated(self):
		return self.authenticated
		#return True
 
	def is_active(self):
		return True
 
	def is_anonymous(self):
		return False

	def get_id(self):
		return str(self.id)  # python 3

	def __repr__(self):
		return '<User %r>' % (self.username)


# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))


class Comment(db.Model):

	__tablename__ = 'comments'

	id = db.Column(db.Integer, primary_key=True)
	comm = db.Column(db.TEXT, nullable=False)

	user_id = db.Column(db.Integer, ForeignKey('users.id'))
	supports = db.relationship("Support", backref='ticket', lazy='dynamic')

	def __init__(self, user_id, comm):
		super(Comment, self).__init__()
		self.user_id = user_id
		self.comm = comm


class Support(db.Model):

	__tablename__ = 'supports'

	id = db.Column(db.Integer, primary_key=True)
	sub = db.Column(db.String(60), index=True)
	msg = db.Column(db.TEXT, nullable=False)
	is_closed = db.Column(db.String(90), default=False)
	priotiy = db.Column(db.String(90), default=False)

	user_id = db.Column(db.Integer, ForeignKey('users.id'))
	comments = db.Column(db.Integer, ForeignKey('comments.id'))

	support_time = db.Column(DateTime, default=datetime.utcnow())

	def __init__(self, sub, msg, priotiy, user_id, is_closed = False):
		super(Support, self).__init__()
		self.sub = sub
		self.msg =msg
		self.user_id = user_id
		self.priotiy = priotiy
		#self.mac = mac
		#self.m3u = m3u

class Reseller(db.Model):

	__tablename__ = "resellers"

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(60), index=True)
	whatsapp = db.Column(db.String(90), index=False)
	country_selector = db.Column(db.String(90), index=False)
	take = db.Column(db.String(90), index=False)
	panel_uri = db.Column(db.String(255), index=False)
	api_key = db.Column(db.String(90), index=False)
	user_id = db.Column(db.Integer, ForeignKey('users.id'))
	time = db.Column(DateTime, default=datetime.utcnow())


	def __init__(self, name, whatsapp, country_selector, take, user_id):
		super(Reseller, self).__init__()
		self.name = name
		self.whatsapp = whatsapp
		self.country_selector = country_selector
		self.take = take
		self.user_id = user_id



class Contact(db.Model):

	__tablename__ = 'contacts'

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(60), index=True)
	mail = db.Column(db.String(60), index=True)
	sub = db.Column(db.String(60), index=True)
	msg = db.Column(db.TEXT, nullable=False)
	is_closed = db.Column(db.String(90), default=False)
	msg_time = db.Column(DateTime, default=datetime.utcnow())

	def __init__(self, name, mail, sub, msg, is_closed = False):
		super(Contact, self).__init__()
		self.name = name
		self.mail = mail
		self.sub = sub
		self.msg = msg
		self.is_closed = is_closed

class Package(db.Model):

	__tablename__ = 'packages'

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(60), index=True)
	img = db.Column(db.String(400), index=True)
	quality = db.Column(db.String(60), index=True)
	channels = db.Column(db.String(60), index=True)
	vod = db.Column(db.String(60), index=True)
	price = db.Column(db.String(60), index=True)
	period = db.Column(db.String(60), index=True)
	slug = db.Column(db.String(180), nullable=False)
	xtream_id = db.Column(db.Integer, index=True)
	pack_time = db.Column(DateTime, default=datetime.utcnow())

	def __init__(self, name, img, quality, channels, price, period, xtream_id):
		super(Package, self).__init__()
		self.name = name
		self.img = img
		self.quality = quality
		self.channels = channels
		self.price = price
		self.period = period
		self.xtream_id = xtream_id
		self.slug = slugify(name)

class Subscription(db.Model):

	__tablename__ = 'subscriptions'

	id = db.Column(db.Integer, primary_key=True)
	f_name = db.Column(db.String(60), index=True)
	email = db.Column(db.String(60), unique=False)
	country_selector = db.Column(db.String(90), index=True)
	pack = db.Column(db.String(90), index=True)
	device_type = db.Column(db.String(60), index=True)
	is_buyed = db.Column(Boolean, unique=False, default=False)
	phone_num = db.Column(db.String(40), unique=False)

	user_id = db.Column(db.Integer, ForeignKey('users.id'))

	username = db.Column(db.String(90), index=True)
	password = db.Column(db.String(90), index=True)

	sub_time = db.Column(DateTime, default=datetime.utcnow())
	

	def __init__(self, f_name, email, pack, country_selector, phone_num, device_type, username,password):
		super(Subscription, self).__init__()
		self.f_name = f_name
		self.email = email
		self.country_selector = country_selector
		self.phone_num = phone_num
		self.device_type = device_type
		self.is_buyed = False
		self.pack = pack
		self.username = username
		self.password = password

		#self.user_id = user_id

class Trial(db.Model):

	__tablename__ = 'trials'

	id = db.Column(db.Integer, primary_key=True)
	country_selector = db.Column(db.String(60), index=True)
	email_rec = db.Column(db.String(60), unique=True)
	device_type = db.Column(db.String(60), index=True)
	trial_time = db.Column(DateTime, default=datetime.utcnow())

	def __init__(self, country_selector, email_rec, device_type):
		super(Trial, self).__init__()
		self.country_selector = country_selector
		self.email_rec = email_rec
		self.device_type = device_type


class Tuto(db.Model):

	__tablename__ = 'tutos'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(100), unique=True)
	txt = db.Column(db.TEXT, nullable=False)
	img = db.Column(db.String(400), index=True)
	tags = db.Column(db.String(255), nullable=False)
	slug = db.Column(db.String(180), nullable=False)
	views = db.Column(db.Integer, default=0)
	user_id = db.Column(db.Integer, ForeignKey('users.id'))
	tuto_time = db.Column(DateTime, default=datetime.utcnow())

	def __init__(self, title, img, tags, txt, user_id):
		super(Tuto, self).__init__()
		self.title = title
		self.txt = txt
		self.img = img
		self.tags = tags
		self.slug = slugify(title)
		self.user_id = user_id


class Temps(db.Model):

	__tablename__ = 'templates'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(60), unique=True)
	img = db.Column(db.String(400), index=True)
	txt = db.Column(db.TEXT, nullable=False)
	is_closed = db.Column(Boolean, unique=False, default=False)
	at = db.Column(DateTime, default=datetime.utcnow())
	user_id = db.Column(db.Integer, ForeignKey('users.id'))


	def __init__(self, title, img, txt, is_closed, user_id):
		super(Temps, self).__init__()
		self.title = title
		self.txt = txt
		self.img = img
		self.is_closed = is_closed
		self.user_id = user_id

class Offer(db.Model):

	__tablename__ = 'offers'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(60), unique=True)
	img = db.Column(db.String(400), index=True)
	txt = db.Column(db.TEXT, nullable=False)
	fin = db.Column(db.String(60), index=True)
	is_closed = db.Column(Boolean, unique=False, default=False)
	coupon = db.Column(db.String(60), unique=True)
	price = db.Column(db.Integer, index=True)
	tags = db.Column(db.String(255), index=True)
	slug = db.Column(db.String(180), nullable=False)
	of_time = db.Column(DateTime, default=datetime.utcnow())

	user_id = db.Column(db.Integer, ForeignKey('users.id'))


	def __init__(self, title, img, txt, fin, coupon, price, tags, user_id):
		super(Offer, self).__init__()
		self.title = title
		self.txt = txt
		self.img = img
		self.fin = fin
		self.coupon = coupon
		self.price = price
		self.tags = tags
		self.slug = slugify(title)
		self.user_id = user_id

