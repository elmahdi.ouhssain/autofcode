# -*- coding: utf-8 -*-
from flask import current_app
from app import mail, Message

#from ..forms import BuyNow, TrialsForm, MagDevice, ContactForm, Getoffer

from threading import Thread, Lock

def send_async_email(app, msg):
	with app.app_context():
		mail.send(msg)
 
def send_email(subject, recipients, html_body):
	msg = Message(subject, sender="URHDIPTV | TEAM", recipients=recipients)
	#msg.body = text_body
	msg.html = html_body
	#thr = Thread(target=send_async_email, args=[current_app, msg])
	#thr.start()
	Thread(target=send_async_email,
		   args=(current_app._get_current_object(), msg)).start()

