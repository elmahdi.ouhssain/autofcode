# -*- coding: utf-8 -*-
from flask import Blueprint, abort, flash, g, jsonify, redirect, \
	render_template, request, url_for, jsonify, json, Markup

from flask_login import login_required

from sqlalchemy.orm import subqueryload
from sqlalchemy import desc, update, insert

from . import news

#from .forms import BuyNow, TrialsForm, MagDevice, ContactForm

from .. import db
#from ..models import User, User, Package, Contact, Livestream, Trial, Tuto, Subscription, Offer

import requests
from newsapi import NewsApiClient


@news.route('/', methods=['GET', 'POST'])
def index():

	newsapi = NewsApiClient(api_key="6cac6e220a9a48339fa548924a324889")
	topheadlines = newsapi.get_top_headlines(category='technology',language='en',country='us')

	articles = topheadlines['articles']
 
	desc = []
	news = []
	img = []

	for i in range(len(articles)):
		myarticles = articles[i]
		news.append(myarticles['title'])
		desc.append(myarticles['description'])
		img.append(myarticles['urlToImage'])
 
	mylist = zip(news, desc, img)

	return render_template('/news/index.html', context = mylist, title='Technology News', description='Latest technology news')


@news.route('/uk', methods=['GET', 'POST'])
def uk():

	newsapi = NewsApiClient(api_key="6cac6e220a9a48339fa548924a324889")
	topheadlines = newsapi.get_top_headlines(category='technology',language='en',country='gb')

	articles = topheadlines['articles']
 
	desc = []
	news = []
	img = []

	for i in range(len(articles)):
		myarticles = articles[i]
 
 
		news.append(myarticles['title'])
		desc.append(myarticles['description'])
		img.append(myarticles['urlToImage'])
 
	mylist = zip(news, desc, img)

	return render_template('/news/uk.html', context = mylist, title='United Kingdom News', description='Latest technology news')

@news.route('/france', methods=['GET', 'POST'])
def france():

	newsapi = NewsApiClient(api_key="6cac6e220a9a48339fa548924a324889")
	topheadlines = newsapi.get_top_headlines(category='technology',language='en',country='fr')

	articles = topheadlines['articles']
 
	desc = []
	news = []
	img = []

	for i in range(len(articles)):
		myarticles = articles[i]
 
 
		news.append(myarticles['title'])
		desc.append(myarticles['description'])
		img.append(myarticles['urlToImage'])
 
	mylist = zip(news, desc, img)

	return render_template('/news/fr.html', context = mylist, title='France news', description='Latest technology news')

@news.route('/germany', methods=['GET', 'POST'])
def germany():

	newsapi = NewsApiClient(api_key="6cac6e220a9a48339fa548924a324889")
	topheadlines = newsapi.get_top_headlines(category='technology',language='en',country='de')

	articles = topheadlines['articles']
 
	desc = []
	news = []
	img = []

	for i in range(len(articles)):
		myarticles = articles[i]
 
		news.append(myarticles['title'])
		desc.append(myarticles['description'])
		img.append(myarticles['urlToImage'])
 
	mylist = zip(news, desc, img)

	return render_template('/news/de.html', context = mylist, title='Germany news', description='Latest technology news')

@news.route('/turkey', methods=['GET', 'POST'])
def turkey():

	newsapi = NewsApiClient(api_key="6cac6e220a9a48339fa548924a324889")
	topheadlines = newsapi.get_top_headlines(category='technology',language='en',country='tr')

	articles = topheadlines['articles']
 
	desc = []
	news = []
	img = []

	for i in range(len(articles)):
		myarticles = articles[i]
 
		news.append(myarticles['title'])
		desc.append(myarticles['description'])
		img.append(myarticles['urlToImage'])
 
	mylist = zip(news, desc, img)

	return render_template('/news/tr.html', context = mylist, title='Turkey news', description='Latest technology news')