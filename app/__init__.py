# third-party imports
from flask import Flask, render_template, g
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, AnonymousUserMixin, current_user
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_ckeditor import CKEditor
from flask_mail import Mail, Message
# local imports
from config import app_config

import os

db = SQLAlchemy()
login_manager = LoginManager()
mail = Mail()


def create_app(config_name):
	app = Flask(__name__, instance_relative_config=True)
	app.config.from_object(app_config['development'])
	app.config.from_pyfile('config.py')
	ckeditor = CKEditor(app)
	Bootstrap(app)
	db.init_app(app)
	mail.init_app(app)

	#Login manager configurations
	login_manager = LoginManager()
	login_manager.init_app(app)
	login_manager.login_view = 'auth.index'
	login_manager.session_protection = "strong"
	login_manager.refresh_view = "auth.index"
	login_manager.needs_refresh_message = (u"To protect your account, please reauthenticate to access this page.")
	login_manager.needs_refresh_message_category = "info"
	migrate = Migrate(app, db)

	from app import models

	#from .panel import panel as panel_blueprint
	#app.register_blueprint(panel_blueprint, url_prefix='/panel')

	from .dash import dash as dash_blueprint
	app.register_blueprint(dash_blueprint, url_prefix='/dash')

	from .auth import auth as auth_blueprint
	app.register_blueprint(auth_blueprint, url_prefix='/auth')

	from .news import news as news_blueprint
	app.register_blueprint(news_blueprint, url_prefix='/news')

	from .home import home as home_blueprint
	app.register_blueprint(home_blueprint, url_prefix='/')

	from app.models import User, Tuto, Offer

	# Set up user_loader
	@login_manager.user_loader
	def load_user(user_id):
		return User.query.get(int(user_id))

	@app.route("/sitemap")
	@app.route("/sitemap/")
	@app.route("/sitemap.xml")
	def sitemap():
		"""
			Route to dynamically generate a sitemap of your website/application.
			lastmod and priority tags omitted on static pages.
			lastmod included on dynamic content such as blog posts.
		"""
		from flask import make_response, request, render_template
		import datetime
		from urllib.parse import urlparse

		host_components = urlparse(request.host_url)
		host_base = host_components.scheme + "://" + host_components.netloc

		# Static routes with static content
		static_urls = list()
		for rule in app.url_map.iter_rules():
			if not str(rule).startswith("/panel") and not str(rule).startswith("/auth"):
				if "GET" in rule.methods and len(rule.arguments) == 0:
					url = {
						"loc": f"{host_base}{str(rule)}"
					}
					static_urls.append(url)

		# Dynamic routes with dynamic content
		dynamic_urls = list()
		dynamic_urls2 = list()

		blog_posts = Tuto.query.all()
		offer_posts = Offer.query.all()

		for offer in offer_posts:
			url2 = {
				"loc": f"{host_base}/offers/{offer.slug}",
				"lastmod": offer.of_time.strftime("%Y-%m-%dT%H:%M:%SZ")
				}
			dynamic_urls2.append(url2)


		for post in blog_posts:
			url = {
				"loc": f"{host_base}/tutorials/{post.slug}",
				"lastmod": post.tuto_time.strftime("%Y-%m-%dT%H:%M:%SZ")
				}
			dynamic_urls.append(url)


		xml_sitemap = render_template("public/sitemap.xml", static_urls=static_urls, dynamic_urls=dynamic_urls, dynamic_urls2=dynamic_urls2, host_base=host_base)
		response = make_response(xml_sitemap)
		response.headers["Content-Type"] = "application/xml"

		return response


	## LIVE VERSION ####
	@app.route('/robots.txt/')
	def robots():
		return("User-agent: *\nDisallow: /login/\nDisallow: /auth/\nDisallow: /panel/")


	@app.errorhandler(404)
	def not_found(error):
		return render_template('public/404.html'), 404

	@app.errorhandler(500)
	def internal_server_error(error):
		return render_template('public/505.html', title='Server Error'), 500

	@app.errorhandler(403)
	def forbidden(error):
		return render_template('public/403.html', title='Forbidden'), 403


	@app.shell_context_processor
	def make_shell_context():
		return {'db.session': db.session}

	@app.teardown_appcontext
	def shutdown_session(exception=None):
		db.session.remove()

	return app




	def ensureUtf(s):
		try:
			if type(s) == unicode:
				return s.encode('utf8', 'ignore')
		except: 
			return str(s)

	