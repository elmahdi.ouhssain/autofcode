# -*- coding: utf-8 -*-
from flask import Blueprint, abort, flash, g, jsonify, redirect, \
	render_template, request, url_for, jsonify, json, Markup, make_response
from flask_login import login_required, login_user, current_user, logout_user
from sqlalchemy.orm import subqueryload
from sqlalchemy import desc, update, insert
from . import dash
from .. import db
from ..models import User, Setting, Comment, Reseller, Temps, Package, Contact, Trial, Tuto, Subscription, Support, Offer, Reseller, init_db, Base, engine
import requests
import json
from threading import Thread, Lock
from flask import current_app
from app import mail, Message
from .forms import Userform, Settingform, NComment, NewTemp, Addpackage, Newuser, Neworder, Newtrial, Newticket, Newtuto, Newoffer, fast_email, UserInfo

import http.client
import smtplib, ssl
import urllib.request
import urllib.parse


def check_admin():

	if not current_user.is_admin:
		abort(403)

def send_async_email(app, msg):
	with app.app_context():
		mail.send(msg)



@dash.route('/index', methods=['GET','POST'])
@login_required
def index():
	check_admin()
	data = {}
	data['ress'] = Reseller.query.order_by(desc(Reseller.id)).limit(4)
	data['messages'] = Contact.query.order_by(desc(Contact.id)).limit(4)
	data['supports'] = Support.query.order_by(desc(Support.id)).limit(4)
	data['users'] = User.query.order_by(desc(User.id)).limit(1)
	data['subscriptions'] = Subscription.query.order_by(desc(Subscription.id)).limit(1)
	data['tutos'] = Tuto.query.all()
	data['tickets'] = Support.query.order_by(desc(Support.id)).limit(1)
	data['ticketts'] = Support.query.order_by(desc(Support.id)).limit(4)
	data['rescount'] = Reseller.query.count()
	data['regcount'] = User.query.count()
	data['subcount'] = Subscription.query.count()
	data['tutocount'] = Tuto.query.count()

	return render_template('/dash/index.html', data=data, title='New Dashboard')

@dash.route('/resellers/show/<int:resellers_id>/', methods=['GET','POST'])
@login_required
def show_resellers(resellers_id):
	check_admin()

	res = Reseller.query.get(resellers_id)

	return render_template('/dash/reseller.html', res = res, title='Resellers Details')

@dash.route('/resellers/newreseller', methods=['GET','POST'])
@login_required
def add_resellers():
	check_admin()


	return render_template('/dash/new_reseller.html', title='New Reseller')

@dash.route('/icons', methods=['GET','POST'])
@login_required
def icons():
	check_admin()

	return render_template('/dash/icons.html', title='New Icons')

@dash.route('/api_setting/<int:api_id>/', methods=['GET','POST'])
@login_required
def api_setting(api_id):
	check_admin()

	apis = Setting.query.get(api_id)
	form = Settingform()
	if form.validate_on_submit():
		apis.api_infos = form.api_infos.data
		apis.api_create = form.api_create.data
		apis.api_mag = form.api_mag.data
		apis.api_enigma = form.api_enigma.data
		apis.m3u = form.m3u.data
		apis.server = form.server.data
		apis.paypal_id = form.paypal_id.data
		db.session.commit()
		#flash('Your Package Has been Updated !', 'success')
		flash('Setting Saved for and pointed to server : {}'.format(apis.server), 'success')
		return redirect(url_for('dash.index', api_id=2))
	elif request.method == 'GET':
		form.api_infos.data = apis.api_infos
		form.api_create.data = apis.api_create
		form.api_mag.data = apis.api_mag
		form.api_enigma.data = apis.api_enigma
		form.m3u.data = apis.m3u
		form.server.data = apis.server
		form.paypal_id.data = apis.paypal_id
	return render_template('/dash/api.html', form=form, title='Api Setting')

@dash.route('/templates/new', methods=['GET','POST'])
@login_required
def newtemp():
	check_admin()

	form = NewTemp()
	if form.validate_on_submit():
		newtemp = Temps(title=form.title.data,txt=form.txt.data,img=form.img.data,is_closed=form.is_closed.data,user_id=current_user.id)
		db.session.add(newtemp)
		db.session.commit()
		flash('Template created for  : {} successfly'.format(newtemp.title), 'primary')
		return redirect(url_for('dash.temps'))

	return render_template('/dash/newtemp.html', form=form, title='New Templates')

@dash.route('/templates/', methods=['GET','POST'])
@login_required
def temps():
	check_admin()
	data = {}
	data['temps'] = Temps.query.all()
	return render_template('/dash/temps.html', data=data, title='Templates messages')

@dash.route('/templates/edit/<int:templates_id>/', methods=['GET', 'POST'])
@login_required
def edittemps(templates_id):
	check_admin()
	templates = Temps.query.get(templates_id)
	form = NewTemp()
	if form.validate_on_submit():
		templates.title = form.title.data
		templates.txt = form.txt.data
		templates.img = form.img.data
		templates.is_closed = form.is_closed.data
		db.session.commit()
		#flash('Your Package Has been Updated !', 'success')
		flash('Template Edited : {}'.format(templates.title), 'success')
		return redirect(url_for('dash.temps', templates_id=templates.id))
	elif request.method == 'GET':
		form.title.data = templates.title
		form.txt.data = templates.txt
		form.img.data = templates.img
		form.is_closed.data = templates.is_closed

	return render_template('/dash/newtemp.html', form=form, title='Edit Template', legend='Edit Tutorial')


@dash.route('/templates/delete/<int:id>/', methods=['GET','POST'])
@login_required
def deltemp(id):
	check_admin()

	temps = Temps.query.get(id)
	db.session.delete(temps)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('Template Deleted : {}'.format(temps.title), 'primary')

	# redirect to the departments page
	return redirect(url_for('dash.temps'))
	return render_template(title="Delete Tutorial")
@dash.route('/registred-users', methods=['GET','POST'])
@login_required
def registred():
	check_admin()
	data = {}
	data['users'] = User.query.all()

	form = Newuser()
	if form.validate_on_submit():
		newuser = User(email=form.email.data,username=form.username.data,password=form.password.data,is_admin=form.is_admin.data,is_reseller=form.is_reseller.data,is_subscriber=form.is_subscriber.data)
		db.session.add(newuser)
		db.session.commit()
		flash('User added : {} successfly'.format(newuser.username), 'primary')
		return redirect(url_for('dash.registred'))
	
	return render_template('/dash/registred-users.html', form = form, data=data, title='Registred users')

@dash.route('/subscriptions', methods=['GET','POST'])
@login_required
def subscriptions():
	check_admin()
	data = {}
	data['subscriptions'] = Subscription.query.all()
	data['packages'] = Package.query.all()

	form = Neworder()
	if form.validate_on_submit():
		newuser = Subscription(f_name=form.f_name.data,email=form.email.data,country_selector=form.country_selector.data,device_type=form.device_type.data,phone_num=form.phone_num.data,pack=request.args.get('pack_id'))
		db.session.add(newuser)
		db.session.commit()
		flash('Subscription created for  : {} successfly'.format(newuser.f_name), 'primary')
		return redirect(url_for('dash.subscriptions'))

	return render_template('/dash/subscription.html', form=form, data=data, title='Subscriptions')

@dash.route('/trials-requests', methods=['GET','POST'])
@login_required
def trials():
	check_admin()
	data = {}
	data['trials'] = Trial.query.all()
	data['packages'] = Package.query.all()

	form = Newtrial()
	if form.validate_on_submit():
		newuser = Trial(country_selector=form.country_selector.data,email_rec=form.email_rec.data,device_type=form.device_type.data)
		db.session.add(newuser)
		db.session.commit()
		flash('Trial created for  : {} successfly'.format(newuser.full_name), 'primary')
		return redirect(url_for('dash.trials'))

	return render_template('/dash/trial.html', form=form, data=data, title='Trial requests')

@dash.route('/trials-requests/delete/<int:id>/', methods=['GET','POST'])
@login_required
def deltrial(id):
	check_admin()

	trials = Trial.query.get(id)
	db.session.delete(trials)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('Trial Deleted : {}'.format(trials.email_rec), 'primary')

	# redirect to the departments page
	return redirect(url_for('dash.trials'))
	return render_template(title="Delete Trial")

@dash.route('/tutorials', methods=['GET','POST'])
@login_required
def tutorials():
	check_admin()
	data = {}
	data['tutos'] = Tuto.query.all()

	return render_template('/dash/tutorial.html', data=data, title='Tutorials')

@dash.route('/tutorials/new', methods=['GET','POST'])
@login_required
def newtuto():
	check_admin()
	form = Newtuto()
	if form.validate_on_submit():
		post = Tuto(title=form.title.data,txt=form.txt.data,img=form.img.data,tags=form.tags.data,user_id=current_user.id)
		db.session.add(post)
		db.session.commit()
		flash('Tutorial created : {}'.format(post.title), 'success')
		return redirect(url_for('dash.tutorials'))

	return render_template('/dash/newtutorial.html', form=form, title='Create Tutorial', legend="New tutorial")

@dash.route('/tutorials/edit/<int:tutorials_id>/', methods=['GET', 'POST'])
@login_required
def edittuto(tutorials_id):
	check_admin()

	tutorials = Tuto.query.get(tutorials_id)
	form = Newtuto()

	if form.validate_on_submit():

		tutorials.title = form.title.data
		tutorials.txt = form.txt.data
		tutorials.img = form.img.data
		tutorials.tags = form.tags.data

		db.session.commit()

		#flash('Your Package Has been Updated !', 'success')
		flash('Tutorial Edited : {}'.format(tutorials.title), 'success')

		return redirect(url_for('dash.tutorials', tutorials_id=tutorials.id))

	elif request.method == 'GET':

		form.title.data = tutorials.title
		form.txt.data = tutorials.txt
		form.img.data = tutorials.img
		form.tags.data = tutorials.tags

	return render_template('/dash/newtutorial.html', form=form, title='Edit Tutorial', legend='Edit Tutorial')

@dash.route('/tutorials/delete/<int:id>/', methods=['GET','POST'])
@login_required
def deltuto(id):
	check_admin()

	tutos = Tuto.query.get(id)
	db.session.delete(tutos)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('Tutorial Deleted : {}'.format(tutos.title), 'primary')

	# redirect to the departments page
	return redirect(url_for('dash.tutorials'))
	return render_template(title="Delete Tutorial")

@dash.route('/tickets', methods=['GET','POST'])
@login_required
def tickets():
	check_admin()
	data = {}
	data['tickets'] = Support.query.all()
	data['users'] = User.query.all()
	form = Newticket()
	if form.validate_on_submit():
		ticket = Support(sub=form.sub.data,msg=form.msg.data,priotiy=form.priotiy.data,user_id=current_user.id)
		db.session.add(ticket)
		db.session.commit()
		flash('Ticket created : {}'.format(ticket.sub), 'success')
		return redirect(url_for('dash.tickets'))

	return render_template('/dash/ticket.html', form=form, data=data, title='Tickets')

@dash.route('/tickets/open/<int:ticket_id>', methods=['GET','POST'])
@login_required
def openticket(ticket_id):
	check_admin()

	ticks = Support.query.get(ticket_id)
	form = NComment()
	if form.validate_on_submit():
		comm = Comment(user_id=current_user.id,ticket_id=ticket_id,comm=form.msg.data)
		db.session.add(comm)
		db.session.commit()
		#msg = Message('Important from URHDIPTV.COM', sender="URHDIPTV.COM", recipients=[form.email.data.strip()])
		#msg.body = form.txt.data.strip()
		#mail.send(msg)
		flash('Ticket Replayed successfully', 'success')
		return redirect(url_for('dash.openticket', ticket_id=ticket_id))

	return render_template('/dash/openticket.html', form=form, ticks=ticks, title='New ticket opened')


@dash.route('/tickets/delete/<int:id>/', methods=['GET','POST'])
@login_required
def delticket(id):
	check_admin()

	tickets = Support.query.get(id)
	db.session.delete(tickets)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('Ticket Deleted : {}'.format(tickets.sub), 'primary')

	# redirect to the departments page
	return redirect(url_for('dash.tickets'))
	return render_template(title="Delete Ticket")

@dash.route('/offers', methods=['GET','POST'])
@login_required
def offers():
	check_admin()

	data = {}
	data['offers'] = Offer.query.all()

	return render_template('/dash/offer.html', data=data, title='Offers')


@dash.route('/offers/edit/<int:offers_id>/', methods=['GET', 'POST'])
@login_required
def editoffer(offers_id):
	check_admin()

	offers = Offer.query.get(offers_id)
	form = Newoffer()

	if form.validate_on_submit():

		offers.title = form.title.data
		offers.img = form.img.data
		offers.txt = form.txt.data
		offers.img = form.img.data
		offers.fin = form.fin.data
		offers.coupon = form.coupon.data
		offers.price = form.price.data
		offers.is_closed = form.is_closed.data
		offers.tags = form.tags.data
		db.session.commit()

		flash('Offer Edited : {}'.format(offers.title), 'success')

		return redirect(url_for('dash.offers', offers_id=offers.id))

	elif request.method == 'GET':

		form.title.data = offers.title
		form.img.data = offers.img
		form.txt.data = offers.txt
		form.img.data = offers.img
		form.fin.data = offers.fin
		form.coupon.data = offers.coupon
		form.price.data = offers.price
		form.is_closed.data = offers.is_closed
		form.tags.data = offers.tags
		
	return render_template('/dash/newoffer.html', legend="Edit offer", form=form, title='Edit Package')



@dash.route('/offers/new', methods=['GET','POST'])
@login_required
def newoffer():
	check_admin()

	form = Newoffer()
	if form.validate_on_submit():

		offer = Offer(title=form.title.data,txt=form.txt.data,img=form.img.data,fin=form.fin.data,coupon=form.coupon.data,price=form.price.data,tags=form.tags.data,user_id=current_user.id)
		db.session.add(offer)
		db.session.commit()
		flash('Offer Saved : {}'.format(offer.title), 'success')
		return redirect(url_for('dash.offers'))

	return render_template('/dash/newoffer.html', legend="New offer", form=form, title='New Offer')

@dash.route('/offers/delete/<int:id>/', methods=['GET','POST'])
@login_required
def deloffer(id):
	check_admin()

	offers = Offer.query.get(id)
	db.session.delete(offers)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('Offer Deleted : {}'.format(offers.title), 'primary')

	# redirect to the departments page
	return redirect(url_for('dash.offers'))
	return render_template(title="Delete Offer")

@dash.route('/packages', methods=['GET','POST'])
@login_required
def packages():
	check_admin()

	data = {}
	data['packages'] = Package.query.order_by(desc(Package.id)).limit(3)

	return render_template('/dash/package.html', data=data, title='Packages')

@dash.route('/packages/new', methods=['GET','POST'])
@login_required
def addpack():

	check_admin()
	form = Addpackage()
	if form.validate_on_submit():

		packages = Package(name=form.name.data,img=form.img.data,period=form.period.data,quality=form.quality.data,channels=form.channels.data,price=form.price.data,xtream_id=form.xtream_id.data)
		db.session.add(packages)
		db.session.commit()
		flash('Package Saved : {}'.format(packages.name), 'success')
		return redirect(url_for('dash.packages'))

	return render_template('/dash/addpack.html', form=form, legend='Add Package', title='New package')

@dash.route('/packages/edit/<int:packages_id>/', methods=['GET', 'POST'])
@login_required
def editpack(packages_id):
	check_admin()

	packages = Package.query.get(packages_id)
	form = Addpackage()

	if form.validate_on_submit():

		packages.name = form.name.data
		packages.img = form.img.data
		packages.period = form.period.data
		packages.quality = form.quality.data
		packages.channels = form.channels.data
		packages.vod = form.vod.data
		packages.price = form.price.data
		packages.xtream_id = form.xtream_id.data

		db.session.commit()

		#flash('Your Package Has been Updated !', 'success')
		flash('Package Edited : {}'.format(packages.name), 'success')

		return redirect(url_for('dash.packages', packages_id=packages.id))

	elif request.method == 'GET':

		form.name.data = packages.name
		form.img.data = packages.img
		form.period.data = packages.period
		form.quality.data = packages.quality
		form.channels.data = packages.channels
		form.vod.data = packages.vod
		form.price.data = packages.price
		form.xtream_id.data = packages.xtream_id
		
	return render_template('/dash/addpack.html', form=form, title='Edit Package', legend='Edit Package')


@dash.route('/packages/delete/<int:id>/', methods=['GET','POST'])
@login_required
def dellpack(id):
	check_admin()

	packages = Package.query.get(id)
	db.session.delete(packages)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('Pacakge Deleted : {}'.format(packages.name), 'primary')

	# redirect to the departments page
	return redirect(url_for('dash.packages'))
	return render_template(title="Delete package")

@dash.route('/inbox/open/<int:msg_id>', methods=['GET','POST'])
@login_required
def openinbox(msg_id):
	check_admin()

	msgs = Contact.query.get(msg_id)
	form = fast_email()
	if form.validate_on_submit():
		msg = Message('Important from URHDIPTV.COM', sender="URHDIPTV.COM", recipients=[form.email.data.strip()])
		msg.body = form.txt.data.strip()
		mail.send(msg)
		flash('Message sent successfly to {}'.format(form.email.data), 'success')
		return redirect(url_for('dash.openinbox', msg_id=msg_id))

	return render_template('/dash/openinbox.html', form=form, msgs=msgs, title='New message Received')

@dash.route('/registred-users/new-user', methods=['GET', 'POST'])
@login_required
def createuser():
	check_admin()

	#form  = Newuser()

	
	return render_template('/dash/createuser.html', form = form, title='Create new user')

@dash.route('/registred-users/editusers/<int:users_id>', methods=['GET','POST'])
@login_required
def editusers(users_id):
	check_admin()

	users = User.query.get(users_id)
	form = Userform()

	if form.validate_on_submit():

		users.credit = form.credit.data
		users.is_admin = form.is_admin.data
		users.is_reseller = form.is_reseller.data
		users.is_subscriber = form.is_subscriber.data
		users.affiliate_code = form.affiliate_code.data
		users.email_confirmed = form.email_confirmed.data
		users.m3u = form.m3u.data
		db.session.commit()

		flash('User Setting Saved For :  {}'.format(users.username), 'success')
		return redirect(url_for('dash.registred', users_id=users.id))
	elif request.method == 'GET':

		form.credit.data = users.credit
		form.is_admin.data = users.is_admin
		form.is_reseller.data = users.is_reseller
		form.is_subscriber.data = users.is_subscriber
		form.affiliate_code.data = users.affiliate_code
		form.email_confirmed.data = users.email_confirmed
		form.m3u.data = users.m3u

	return render_template('/dash/profile.html', users=users, form=form, title='Manage User')

@dash.route('/registred-users/editusers/delete/<int:id>/', methods=['GET', 'POST'])
@login_required
def deluser(id):
	check_admin()

	users = User.query.get(id)
	db.session.delete(users)
	db.session.commit()
	#flash('You have successfully delete User', "success")
	flash('User Deleted : {}'.format(users.username), 'success')

	# redirect to the departments page
	return redirect(url_for('dash.registred'))
	return render_template(title="Delete User")

	
@dash.route('/setting/<id>', methods=['GET', 'POST'])
@login_required
def setting(id):
	check_admin()
	users = User.query.filter_by(id=id).one()
	form = UserInfo()
	if request.method == 'POST':
		if form.validate_on_submit():
			users = current_user
			if users.verify_password(form.current_password.data):
				users.password = form.new_password.data
				db.session.add(users)
				db.session.commit()
				flash('Password has been updated Mr :  {}'.format(current_user.username), 'success')
				return redirect(url_for('dash.setting', id=current_user.id))
			else:
				flash('Current password incorrect !.', 'danger')
	return render_template('/dash/setting.html', users=users, form=form, title='Account Setting')


@dash.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
	check_admin()
	user = current_user
	user.authenticated = False
	db.session.add(user)
	db.session.commit()
	logout_user()
	flash("You are now loggin out !!", 'success')
	return redirect(url_for('home.homepage'))
