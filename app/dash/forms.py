from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, IntegerField, PasswordField, BooleanField, SubmitField, validators, SelectField, TextAreaField, DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from flask_ckeditor import CKEditorField 
from ..models import User, Package, Contact, Trial, Tuto, Subscription, Offer, Temps

class Settingform(FlaskForm):

	api_infos = StringField('Getting Infos : ')
	api_create = StringField('Create Newline : ')
	api_mag = StringField('Create Mag device Api : ')
	api_enigma = StringField('Create Enigma device Api : ')
	m3u = StringField('M3u : ')
	server = StringField('Main Server : ')
	paypal_id = StringField('Paypal Identifier : ')


	#recaptcha = RecaptchaField()
	submit = SubmitField('Save')


class UserInfo(FlaskForm):
	current_password = PasswordField('Current Password', validators=[DataRequired(), validators.Length(min=4)])

	new_password = PasswordField(validators=[
										DataRequired(),
										validators.Length(min=4),
										EqualTo('confirm_password')
										])

	confirm_password = PasswordField('Confirm Password')

	#recaptcha = RecaptchaField()

	submit = SubmitField('Change Password')

class fast_email(FlaskForm):

	email = StringField('Email', validators=[DataRequired(), Email()])

	txt = CKEditorField('Message')

	#recaptcha = RecaptchaField()
	
	submit = SubmitField('Send')

class Newoffer(FlaskForm):

	title = StringField('Title', validators=[DataRequired()])

	txt = CKEditorField('Text', validators=[DataRequired()])

	img = StringField('Image url', validators=[DataRequired()])

	fin = StringField('Fin Date', validators=[DataRequired()])

	coupon = StringField('Coupon', validators=[DataRequired()])

	price = StringField('Price', validators=[DataRequired()])

	is_closed  = BooleanField('Make this Offer Active', default=False)

	tags = StringField('Tags', validators=[DataRequired()])

	submit = SubmitField('Save')

class NewTemp(FlaskForm):

	title = StringField('Title', validators=[DataRequired()])

	txt = CKEditorField('Description', validators=[DataRequired()])

	img = StringField('Image url')

	is_closed = BooleanField('ON/OFF')

	submit = SubmitField('Save')

	def validate_name(self, title):
		temps = Temps.query.filter_by(title=title.data).first()
		if temps is not None:
			raise ValidationError('Please use a different Title.')

class Newtuto(FlaskForm):

	title = StringField('Title', validators=[DataRequired()])

	txt = CKEditorField('Description', validators=[DataRequired()])

	img = StringField('Image url', validators=[DataRequired()])

	tags = StringField('Tags', validators=[DataRequired()])

	submit = SubmitField('Save')

	def validate_name(self, title):
		tutorial = Tuto.query.filter_by(title=title.data).first()
		if tutorial is not None:
			raise ValidationError('Please use a different Title.')

class NComment(FlaskForm):

	msg = TextAreaField('Message', validators=[DataRequired()])
	submit = SubmitField('Send')
	#recaptcha = RecaptchaField(

class Newticket(FlaskForm):

	sub = SelectField('Subject', choices=[('Need help', 'Need help'),('Reseller program', 'Reseller program'), ('Affilate program', 'Affilate program'), ('Ask for sales', 'Ask for sales'), ('Autre', 'Autre')])
	priotiy = SelectField('Priotiy', choices=[('HIGHT', 'HIGHT'), ('MEDIUM', 'MEDIUM'), ('LOW', 'LOW')])
	msg = TextAreaField('Message', validators=[DataRequired()])
	#recaptcha = RecaptchaField()
	submit = SubmitField('Open Ticket')

class Newtrial(FlaskForm):

	country_selector = StringField('Full name', validators=[DataRequired()])
	email_rec = StringField('Email', validators=[DataRequired(), Email()])
	device_type = SelectField('Device Type', choices=[('Android', 'Android'), ('Smart Tv', 'Smart Tv'), ('Receiver', 'Receiver'), ('Apple Tv', 'Apple Tv'), ('PC', 'PC')])

	#recaptcha = RecaptchaField()

	#submit = SubmitField('Request')

	def validate_email_rec(self, email_rec):
		trial = Trial.query.filter_by(email_rec=email_rec.data).first()
		if trial is not None:
			raise ValidationError('Trial already sent to you , please check your inbox.')


class Neworder(FlaskForm):
	
	f_name = StringField('Full Name')

	email = StringField('Email', validators=[DataRequired(), Email()])

	#confirm_email = StringField('Confirm Email', validators=[DataRequired(),EqualTo('confirm_email')])

	country_selector = StringField('Location')

	device_type = SelectField('Device Type', choices=[('ANDROID', 'Android'), ('SMART TV', 'Smart Tv'), ('RECEIVER', 'Receiver'), ('APPLE TV', 'Apple Tv'), ('PC', 'PC'), ('Mag', 'Mag'), ('Enigma', 'Enigma')])

	phone_num = StringField('Phone Number')

	#cuppon = StringField('Coupon *not required*', validators=None)

	note = TextAreaField('Note About Order')

	#recaptcha = RecaptchaField()
	
	submit = SubmitField('Create order')

class Newuser(FlaskForm):

	email = StringField('Email', validators=[DataRequired(), Email()])

	username = StringField(validators=[DataRequired()])

	password = PasswordField(validators=[DataRequired(),validators.Length(min=10)])

	is_admin  = BooleanField('Add User To Administrators', default=False)

	is_reseller  = BooleanField('Add User To Resellers', default=False)

	is_subscriber = BooleanField('Add User to Subscribers', default=False)

	is_active = BooleanField('Block user', default=False)


	submit = SubmitField('Save')


class Email_send(FlaskForm):

	email = StringField('Email', validators=[DataRequired(), Email()])

	recaptcha = RecaptchaField()

	submit = SubmitField('Send')

class Userform(FlaskForm):

	credit = StringField('Credit')

	is_admin  = BooleanField('Add User To Administrators', default=False)

	is_reseller  = BooleanField('Add User To Resellers', default=False)

	is_subscriber = BooleanField('Add User to Subscribers', default=False)

	is_active = BooleanField('Block user', default=False)

	#status = SelectField('Status of User', choices=[('R', '12'), ('06', '06'), ('03', '03')])

	affiliate_code = StringField('Affiliate Code')

	m3u = StringField('M3u Link')

	email_confirmed = BooleanField('Confirm Email of User', default=False)

	submit = SubmitField('Save')

class Addpackage(FlaskForm):

	name = StringField('Package Name', validators=[DataRequired()])

	img = StringField('Image url', validators=[DataRequired()])

	period = SelectField('Period / Months', choices=[('12', '12'), ('06', '06'), ('03', '03')])

	quality = SelectField('Quality', choices=[('4K|HD|SD|LOW', '4K|HD|SD|LOW'), ('HD|SD|LOW', 'HD|SD|LOW')])

	channels = StringField('Channels', validators=[DataRequired()])

	vod = SelectField('Type Vod', choices=[('Yes', 'yes'), ('No', 'no')])

	price = StringField('Price $', validators=[DataRequired()])

	xtream_id = IntegerField('ID of xtream-ui package')

	submit = SubmitField('Add Package')