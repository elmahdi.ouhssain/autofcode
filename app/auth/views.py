# -*- coding: utf-8 -*-
from flask import Blueprint, abort, flash, g, jsonify, redirect, \
	render_template, request, url_for, jsonify, json, Markup

from flask_login import login_required, login_user, current_user, logout_user
from sqlalchemy.orm import subqueryload
from sqlalchemy import desc, update, insert, func

from . import auth
from .forms import Email_send, Reset_email, URLoginForm, URRegistrationForm, SendInfos, SupportForm, Emailchange, UserInfo

from .. import db
from ..models import User, User, Package, Contact, Trial, Tuto, Subscription, Offer, Support

from ..sender import *
import os
from datetime import datetime, timedelta
from itsdangerous import URLSafeTimedSerializer
from threading import Thread, Lock
from sqlalchemy.exc import IntegrityError

def send_password_reset_email(user_email):
	
	password_reset_serializer = URLSafeTimedSerializer('p9Bv<3Eid9%$i0176d51312bb3eef39100f64ba5c718')
 
	password_reset_url = url_for('auth.reset_with_token',token = password_reset_serializer.dumps(user_email, salt='password-reset-salt'),_external=True)
 
	html = render_template('/auth/users/email_password_reset.html',password_reset_url=password_reset_url)
 
	send_email('Password Reset Requested', [user_email], html)


@auth.route('/reset', methods=["GET", "POST"])
def reset():
	form = Email_send()
	if form.validate_on_submit():
		try:
			user = User.query.filter_by(email=form.email.data).first()
		except:
			flash('Invalid email address!', 'danger')
			return render_template('/auth/users/password_reset_email.html', form=form)
		 
		if user.email_confirmed is True:
			send_password_reset_email(user.email)
			flash('Please check your email for a password reset link.', 'success')
		else:
			flash('Your email address is not confirmed or not registred.', 'danger')
		return redirect(url_for('auth.index'))
 
	return render_template('/auth/users/password_reset_email.html', form=form)


@auth.route('/reset/<token>', methods=["GET", "POST"])
def reset_with_token(token):
	try:
		password_reset_serializer = URLSafeTimedSerializer('p9Bv<3Eid9%$i0176d51312bb3eef39100f64ba5c718')
		email = password_reset_serializer.loads(token, salt='password-reset-salt', max_age=3600)
	except:
		flash('The password reset link is invalid or has expired.', 'danger')
		return redirect(url_for('auth.index'))
 
	form = Reset_email()
	if form.validate_on_submit():
		try:
			user = User.query.filter_by(email=email).first()
		except:
			flash('Invalid email address!', 'danger')
			return redirect(url_for('auth.index'))
 
		user.password = form.password.data
		db.session.add(user)
		db.session.commit()
		flash('Your password has been updated !', 'success')
		return redirect(url_for('auth.index'))
 
	return render_template('/auth/users/reset_password_with_token.html', form=form, token=token)
############################################################################################################


def send_confirmation_email(user_email):

	confirm_serializer = URLSafeTimedSerializer('p9Bv<3Eid9%$i0176d51312bb3eef39100f64ba5c718')
 
	confirm_url = url_for('auth.confirm_email',token=confirm_serializer.dumps(user_email, salt='email-confirmation-salt'),_external=True)
 
	html = render_template('/auth/users/email_confirmation.html', confirm_url=confirm_url)
 
	send_email('Confirm Your Email Address', [user_email], html)


@auth.route('/confirm/<token>', methods=['GET', 'POST'])
def confirm_email(token):
	try:
		confirm_serializer = URLSafeTimedSerializer('p9Bv<3Eid9%$i0176d51312bb3eef39100f64ba5c718')
		email = confirm_serializer.loads(token, salt='email-confirmation-salt', max_age=3600)
	except:
		flash('The confirmation link is invalid or has expired.', 'danger')
		return redirect(url_for('auth.index'))
 
	user = User.query.filter_by(email=email).first()
 
	if user.email_confirmed is True:
		flash('Your account is already confirmed. Please login.', 'info')
	else:
		user.email_confirmed = True
		user.email_confirmed_on = datetime.now()
		db.session.add(user)
		db.session.commit()
		flash('Your email is now confirmed', 'success')
 
	return redirect(url_for('auth.index'))

@auth.route('/resend_confirmation')
@login_required
def resend_email_confirmation():
	try:
		send_confirmation_email(current_user.email)
		flash('Email sent for confirmation, Please check your Inbox!', 'success')
	except IntegrityError:
		flash('Error! Unable to send email confirmation to your email address.', 'danger')
	return redirect(url_for('auth.dashboard', username=current_user.username))

###################################################################################################

@auth.route('/login', methods=['GET', 'POST'])
def index():

	form = URLoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user is not None and user.verify_password(form.password.data):
			user.authenticated = True
			user.last_login_at = user.current_login_at
			user.current_login_at = datetime.now()
			db.session.add(user)
			db.session.commit()
			login_user(user, remember=form.remember.data)
			if user.is_admin:
				return redirect(url_for('dash.index'))
				flash('Welcome To URHDIPTV Admin :  {}'.format(current_user.username), 'success')
			else:
				return redirect(url_for('auth.dashboard'))
				flash('Welcome To URHDIPTV Please select your package Mr:  {}'.format(current_user.username), 'success')
			#return redirect(url_for('auth.dashboad'))
		else:
			flash('Error! Incorrect login credentials.', 'danger')
	return render_template('security/login_user.html', form=form, title='Sign in')

@auth.route('/register', methods=['GET', 'POST'])
def register():

	form = URRegistrationForm()
	if form.validate_on_submit():
		user = User(email=form.email.data,username=form.username.data,password=form.password.data)
		db.session.add(user)
		db.session.commit()
		send_confirmation_email(user.email)
		flash('You are now registered please log in', 'success')
		# redirect to the login page
		return redirect(url_for('auth.index'))

	return render_template('security/register_user.html', form=form, title='Signup')

@auth.route('/dashboard/<username>/', methods=['GET', 'POST'])
@login_required
def dashboard(username):

	user = User.query.filter_by(username=username).one()
	subs = Subscription.query.filter_by(user_id=current_user.id).first()
	supports = Support.query.filter_by(user_id=current_user.id).first()
	
	return render_template('auth/users/dashboard.html', user=user, subs=subs, supports=supports,  title='Dashboard Users')


@auth.route('/dashboard/sent_infos/<int:infos_id>/', methods=['GET', 'POST'])
@login_required
def sent_infos(infos_id):

	subs = Subscription.query.filter_by(user_id=current_user.id).first()
	supports = Support.query.filter_by(user_id=current_user.id).first()

	infos = User.query.get(infos_id)
	form = SendInfos()
	if form.validate_on_submit():
		infos.mac = form.mac.data
		infos.channel = form.channel.data
		db.session.commit()
		#flash('Your Tutorial Has been Updated !', 'success')
		flash('Your Mac Adress & channel is now Saved : {}'.format(current_user.username), 'success')
		return redirect(url_for('auth.dashboard', username=current_user.username))
	return render_template('auth/users/request.html', form=form, subs=subs, title='Send TV Infos')


@auth.route('/dashboard/bereseller/', methods=['GET', 'POST'])
@login_required
def reseller():

	subs = Subscription.query.filter_by(user_id=current_user.id).first()
	return render_template('auth/users/reseller.html', subs=subs, title='Dashboard Tasks')

@auth.route('/dashboard/help/', methods=['GET', 'POST'])
@login_required
def help():
	subs = Subscription.query.filter_by(user_id=current_user.id).first()
	form = SupportForm()
	if form.validate_on_submit():
		support = Support(msg=form.msg.data,sub=form.sub.data,user_id=current_user.id)
		db.session.add(support)
		db.session.commit()
		flash('Your Ticket is Created Mr :  {}'.format(current_user.username), 'success')

		return redirect(url_for('auth.help'))

	return render_template('auth/users/help.html', subs=subs, form=form, title='Support Center')

@auth.route('/dashboard/<username>/setting/', methods=['GET', 'POST'])
@login_required
def edituser(username):

	subs = Subscription.query.filter_by(user_id=current_user.id).first()

	#users = User.query.get(users_id)
	users = User.query.filter_by(username=username).one()
	form = UserInfo()
	if request.method == 'POST':
		if form.validate_on_submit():
			users = current_user
			if users.verify_password(form.current_password.data):
				users.password = form.new_password.data
				db.session.add(users)
				db.session.commit()
				flash('Password has been updated Mr :  {}'.format(current_user.username), 'success')
				return redirect(url_for('auth.edituser', username=current_user.username))
			else:
				flash('Current password incorrect !.', 'danger')
	return render_template('/auth/users/setting.html', subs=subs, form=form, title='Account Setting')

@auth.route('/dashboard/invite/', methods=['GET', 'POST'])
@login_required
def invite():
	subs = Subscription.query.filter_by(user_id=current_user.id).first()
	return render_template('auth/users/invite.html', subs=subs, title='Affilate Program')


@auth.route('/dashboard/<username>/change_mail/', methods=['GET', 'POST'])
@login_required
def change_mail(username):

	subs = Subscription.query.filter_by(user_id=current_user.id).first()

	users = User.query.filter_by(username=username).one()
	form = Emailchange()
	if request.method == 'POST':
		if form.validate_on_submit():
			try:
				user_check = User.query.filter_by(email=form.mail.data).first()
				if user_check is None:
					user = current_user
					user.email = form.mail.data
					user.email_confirmed = False
					user.email_confirmed_on = None
					user.email_confirmation_sent_on = datetime.now()
					db.session.add(user)
					db.session.commit()
					send_confirmation_email(user.email)
					flash('Email changed!  Please confirm your new email address ( *link sent to new email* ).', 'success')
					return redirect(url_for('auth.change_mail', username=current_user.username))
				else:
					flash('Sorry, that email already exists!', 'danger')
					
			except IntegrityError:
				flash('Error! That email already exists!', 'danger')

	return render_template('auth/users/change_mail.html', users=users, subs=subs, form=form, title='Change email')


@auth.route('/logout/', methods=['GET', 'POST'])
@login_required
def logout():
	
	user = current_user
	user.authenticated = False
	db.session.add(user)
	db.session.commit()
	logout_user()
	flash('You are now loggin out', 'success')
	return redirect(url_for('home.homepage'))
