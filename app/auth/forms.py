from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, PasswordField, BooleanField, SubmitField, validators, SelectField, TextAreaField, DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
#from fcode.database import User, Package, Contact, Subscription, Trial, Tuto, Offer, db_session, init_db, Base, engine, db_session
from flask_ckeditor import CKEditorField 
#from flask_security.forms import LoginForm, RegisterForm


from ..models import User, User, Package, Contact, Trial, Tuto, Subscription, Offer


class Email_send(FlaskForm):

	email = StringField('Email', validators=[DataRequired(), Email()])

	recaptcha = RecaptchaField()

	submit = SubmitField('Send')

class Reset_email(FlaskForm):

	password = PasswordField(validators=[
											DataRequired(),
											validators.Length(min=4),
											EqualTo('confirm_password')
											])

	confirm_password = PasswordField('Confirm Password')


	submit = SubmitField('Reset Password')


class URLoginForm(FlaskForm):

	email = StringField('Email', validators=[DataRequired(), Email()])

	password = PasswordField('Password', validators=[DataRequired(), validators.Length(min=4)])
	
	remember = BooleanField("Remember Me", default=False)

	#recaptcha = RecaptchaField()

	submit = SubmitField('Login')


class URRegistrationForm(FlaskForm):

	email = StringField(validators=[DataRequired(), Email()])

	username = StringField(validators=[DataRequired()])

	password = PasswordField(validators=[
											DataRequired(),
											validators.Length(min=10),
											EqualTo('confirm_password')
											])

	confirm_password = PasswordField('Confirm Password')

	#recaptcha = RecaptchaField()

	submit = SubmitField('Register')

	def validate_email(self, email):
		user = User.query.filter_by(email=email.data).first()
		if user is not None:
			raise ValidationError('Please use a different email.')

	def validate_username(self, username):
		user = User.query.filter_by(username=username.data).first()
		if user is not None:
			raise ValidationError('Please use a different username.')


class UserInfo(FlaskForm):


	current_password = PasswordField('Current Password', validators=[DataRequired(), validators.Length(min=4)])

	new_password = PasswordField(validators=[
										DataRequired(),
										validators.Length(min=4),
										EqualTo('confirm_password')
										])

	confirm_password = PasswordField('Confirm Password')

	recaptcha = RecaptchaField()

	submit = SubmitField('Change Password')


class SupportForm(FlaskForm):

	#sub = StringField('Subject', validators=[DataRequired()])

	sub = SelectField('Subject', choices=[('Need help', 'Need help'),('Reseller program', 'Reseller program'), ('Affilate program', 'Affilate program'), ('Ask for sales', 'Ask for sales'), ('Autre', 'Autre')])

	msg = TextAreaField('Message', validators=[DataRequired()])

	#mail = StringField('Email',validators=None)

	recaptcha = RecaptchaField()

	submit = SubmitField('Open Ticket')

class Emailchange(FlaskForm):

	mail = StringField('Email (* must be valid *)', validators=[DataRequired(), Email()])

	recaptcha = RecaptchaField()

	submit = SubmitField('Change')

class SendInfos(FlaskForm):

	mac = StringField('Enter Your mac Address : ', validators=[DataRequired(), validators.Length(min=4)])

	channel =  StringField('Add your channel : ', validators=[DataRequired(), validators.Length(min=2)])

	recaptcha = RecaptchaField()

	submit = SubmitField('Send')