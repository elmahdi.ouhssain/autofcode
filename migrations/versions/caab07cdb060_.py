"""empty message

Revision ID: caab07cdb060
Revises: d5faae8227e4
Create Date: 2021-02-20 16:11:10.221663

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'caab07cdb060'
down_revision = 'd5faae8227e4'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('subscriptions', sa.Column('user_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'subscriptions', 'users', ['user_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'subscriptions', type_='foreignkey')
    op.drop_column('subscriptions', 'user_id')
    # ### end Alembic commands ###
