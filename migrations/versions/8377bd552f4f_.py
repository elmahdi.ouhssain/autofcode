"""empty message

Revision ID: 8377bd552f4f
Revises: 
Create Date: 2021-02-13 17:14:36.247724

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8377bd552f4f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('contacts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=60), nullable=True),
    sa.Column('mail', sa.String(length=60), nullable=True),
    sa.Column('sub', sa.String(length=60), nullable=True),
    sa.Column('msg', sa.TEXT(), nullable=False),
    sa.Column('is_closed', sa.String(length=90), nullable=True),
    sa.Column('msg_time', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_contacts_mail'), 'contacts', ['mail'], unique=False)
    op.create_index(op.f('ix_contacts_name'), 'contacts', ['name'], unique=False)
    op.create_index(op.f('ix_contacts_sub'), 'contacts', ['sub'], unique=False)
    op.create_table('packages',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=60), nullable=True),
    sa.Column('img', sa.String(length=400), nullable=True),
    sa.Column('quality', sa.String(length=60), nullable=True),
    sa.Column('channels', sa.String(length=60), nullable=True),
    sa.Column('vod', sa.String(length=60), nullable=True),
    sa.Column('price', sa.String(length=60), nullable=True),
    sa.Column('period', sa.String(length=60), nullable=True),
    sa.Column('slug', sa.String(length=180), nullable=False),
    sa.Column('xtream_id', sa.Integer(), nullable=True),
    sa.Column('pack_time', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_packages_channels'), 'packages', ['channels'], unique=False)
    op.create_index(op.f('ix_packages_img'), 'packages', ['img'], unique=False)
    op.create_index(op.f('ix_packages_name'), 'packages', ['name'], unique=False)
    op.create_index(op.f('ix_packages_period'), 'packages', ['period'], unique=False)
    op.create_index(op.f('ix_packages_price'), 'packages', ['price'], unique=False)
    op.create_index(op.f('ix_packages_quality'), 'packages', ['quality'], unique=False)
    op.create_index(op.f('ix_packages_vod'), 'packages', ['vod'], unique=False)
    op.create_index(op.f('ix_packages_xtream_id'), 'packages', ['xtream_id'], unique=False)
    op.create_table('settings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('api_infos', sa.String(length=400), nullable=True),
    sa.Column('api_create', sa.String(length=400), nullable=True),
    sa.Column('api_mag', sa.String(length=400), nullable=True),
    sa.Column('last_check', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_settings_api_create'), 'settings', ['api_create'], unique=False)
    op.create_index(op.f('ix_settings_api_infos'), 'settings', ['api_infos'], unique=False)
    op.create_index(op.f('ix_settings_api_mag'), 'settings', ['api_mag'], unique=False)
    op.create_table('trials',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('country_selector', sa.String(length=60), nullable=True),
    sa.Column('email_rec', sa.String(length=60), nullable=True),
    sa.Column('device_type', sa.String(length=60), nullable=True),
    sa.Column('trial_time', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email_rec')
    )
    op.create_index(op.f('ix_trials_country_selector'), 'trials', ['country_selector'], unique=False)
    op.create_index(op.f('ix_trials_device_type'), 'trials', ['device_type'], unique=False)
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(length=60), nullable=True),
    sa.Column('username', sa.String(length=60), nullable=True),
    sa.Column('password_hash', sa.String(length=255), nullable=True),
    sa.Column('credit', sa.Integer(), nullable=True),
    sa.Column('is_admin', sa.Boolean(), nullable=True),
    sa.Column('is_reseller', sa.Boolean(), nullable=True),
    sa.Column('is_subscriber', sa.Boolean(), nullable=True),
    sa.Column('affiliate_code', sa.String(length=90), nullable=True),
    sa.Column('last_login_at', sa.DateTime(), nullable=True),
    sa.Column('current_login_at', sa.DateTime(), nullable=True),
    sa.Column('last_login_ip', sa.String(length=100), nullable=True),
    sa.Column('current_login_ip', sa.String(length=100), nullable=True),
    sa.Column('login_count', sa.Integer(), nullable=True),
    sa.Column('channel', sa.String(length=90), nullable=True),
    sa.Column('mac', sa.String(length=90), nullable=True),
    sa.Column('m3u', sa.TEXT(), nullable=True),
    sa.Column('email_confirmation_sent_on', sa.DateTime(), nullable=True),
    sa.Column('email_confirmed', sa.Boolean(), nullable=True),
    sa.Column('email_confirmed_on', sa.DateTime(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('mac'),
    sa.UniqueConstraint('username')
    )
    op.create_table('comments',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('comm', sa.TEXT(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('offers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=60), nullable=True),
    sa.Column('img', sa.String(length=400), nullable=True),
    sa.Column('txt', sa.TEXT(), nullable=False),
    sa.Column('fin', sa.String(length=60), nullable=True),
    sa.Column('is_closed', sa.Boolean(), nullable=True),
    sa.Column('coupon', sa.String(length=60), nullable=True),
    sa.Column('price', sa.Integer(), nullable=True),
    sa.Column('tags', sa.String(length=255), nullable=True),
    sa.Column('slug', sa.String(length=180), nullable=False),
    sa.Column('of_time', sa.DateTime(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('coupon'),
    sa.UniqueConstraint('title')
    )
    op.create_index(op.f('ix_offers_fin'), 'offers', ['fin'], unique=False)
    op.create_index(op.f('ix_offers_img'), 'offers', ['img'], unique=False)
    op.create_index(op.f('ix_offers_price'), 'offers', ['price'], unique=False)
    op.create_index(op.f('ix_offers_tags'), 'offers', ['tags'], unique=False)
    op.create_table('resellers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=60), nullable=True),
    sa.Column('whatsapp', sa.String(length=90), nullable=True),
    sa.Column('country_selector', sa.String(length=90), nullable=True),
    sa.Column('take', sa.String(length=90), nullable=True),
    sa.Column('panel_uri', sa.String(length=255), nullable=True),
    sa.Column('api_key', sa.String(length=90), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_resellers_name'), 'resellers', ['name'], unique=False)
    op.create_table('subscriptions',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('f_name', sa.String(length=60), nullable=True),
    sa.Column('email', sa.String(length=60), nullable=True),
    sa.Column('country_selector', sa.String(length=90), nullable=True),
    sa.Column('pack', sa.String(length=90), nullable=True),
    sa.Column('device_type', sa.String(length=60), nullable=True),
    sa.Column('is_buyed', sa.Boolean(), nullable=True),
    sa.Column('phone_num', sa.String(length=40), nullable=True),
    sa.Column('note', sa.String(length=90), nullable=True),
    sa.Column('sub_time', sa.DateTime(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_subscriptions_country_selector'), 'subscriptions', ['country_selector'], unique=False)
    op.create_index(op.f('ix_subscriptions_device_type'), 'subscriptions', ['device_type'], unique=False)
    op.create_index(op.f('ix_subscriptions_f_name'), 'subscriptions', ['f_name'], unique=False)
    op.create_index(op.f('ix_subscriptions_note'), 'subscriptions', ['note'], unique=False)
    op.create_index(op.f('ix_subscriptions_pack'), 'subscriptions', ['pack'], unique=False)
    op.create_table('templates',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=60), nullable=True),
    sa.Column('img', sa.String(length=400), nullable=True),
    sa.Column('txt', sa.TEXT(), nullable=False),
    sa.Column('is_closed', sa.Boolean(), nullable=True),
    sa.Column('at', sa.DateTime(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('title')
    )
    op.create_index(op.f('ix_templates_img'), 'templates', ['img'], unique=False)
    op.create_table('tutos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=100), nullable=True),
    sa.Column('txt', sa.TEXT(), nullable=False),
    sa.Column('img', sa.String(length=400), nullable=True),
    sa.Column('tags', sa.String(length=255), nullable=False),
    sa.Column('slug', sa.String(length=180), nullable=False),
    sa.Column('views', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('tuto_time', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('title')
    )
    op.create_index(op.f('ix_tutos_img'), 'tutos', ['img'], unique=False)
    op.create_table('supports',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('sub', sa.String(length=60), nullable=True),
    sa.Column('msg', sa.TEXT(), nullable=False),
    sa.Column('is_closed', sa.String(length=90), nullable=True),
    sa.Column('priotiy', sa.String(length=90), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('comments', sa.Integer(), nullable=True),
    sa.Column('support_time', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['comments'], ['comments.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_supports_sub'), 'supports', ['sub'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_supports_sub'), table_name='supports')
    op.drop_table('supports')
    op.drop_index(op.f('ix_tutos_img'), table_name='tutos')
    op.drop_table('tutos')
    op.drop_index(op.f('ix_templates_img'), table_name='templates')
    op.drop_table('templates')
    op.drop_index(op.f('ix_subscriptions_pack'), table_name='subscriptions')
    op.drop_index(op.f('ix_subscriptions_note'), table_name='subscriptions')
    op.drop_index(op.f('ix_subscriptions_f_name'), table_name='subscriptions')
    op.drop_index(op.f('ix_subscriptions_device_type'), table_name='subscriptions')
    op.drop_index(op.f('ix_subscriptions_country_selector'), table_name='subscriptions')
    op.drop_table('subscriptions')
    op.drop_index(op.f('ix_resellers_name'), table_name='resellers')
    op.drop_table('resellers')
    op.drop_index(op.f('ix_offers_tags'), table_name='offers')
    op.drop_index(op.f('ix_offers_price'), table_name='offers')
    op.drop_index(op.f('ix_offers_img'), table_name='offers')
    op.drop_index(op.f('ix_offers_fin'), table_name='offers')
    op.drop_table('offers')
    op.drop_table('comments')
    op.drop_table('users')
    op.drop_index(op.f('ix_trials_device_type'), table_name='trials')
    op.drop_index(op.f('ix_trials_country_selector'), table_name='trials')
    op.drop_table('trials')
    op.drop_index(op.f('ix_settings_api_mag'), table_name='settings')
    op.drop_index(op.f('ix_settings_api_infos'), table_name='settings')
    op.drop_index(op.f('ix_settings_api_create'), table_name='settings')
    op.drop_table('settings')
    op.drop_index(op.f('ix_packages_xtream_id'), table_name='packages')
    op.drop_index(op.f('ix_packages_vod'), table_name='packages')
    op.drop_index(op.f('ix_packages_quality'), table_name='packages')
    op.drop_index(op.f('ix_packages_price'), table_name='packages')
    op.drop_index(op.f('ix_packages_period'), table_name='packages')
    op.drop_index(op.f('ix_packages_name'), table_name='packages')
    op.drop_index(op.f('ix_packages_img'), table_name='packages')
    op.drop_index(op.f('ix_packages_channels'), table_name='packages')
    op.drop_table('packages')
    op.drop_index(op.f('ix_contacts_sub'), table_name='contacts')
    op.drop_index(op.f('ix_contacts_name'), table_name='contacts')
    op.drop_index(op.f('ix_contacts_mail'), table_name='contacts')
    op.drop_table('contacts')
    # ### end Alembic commands ###
